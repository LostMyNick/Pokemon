import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {PokemonList} from "./class/pokemon_list";
import {Pokemon} from "./class/pokemon";

import {RootObject} from "./class/pokemon_response";
import {Type} from "./class/pokemon_type";
import {PokemonAbility} from "./class/pokemon_ability";
import {PokemonMove} from "./class/pokemon_move";
import {PokemonStats} from "./class/pokemon_stats";

@Injectable({
  providedIn: 'root'
})
export class RepositoryService {

  constructor(private http: HttpClient) {
  }

  url = 'https://pokeapi.co/api/v2/'

  async getPokemon(offset: number, limit: number): Promise<Pokemon[]> {
    let res = await this.http.get<PokemonList>(this.url + 'pokemon/', {
      params: {
        offset: offset,
        limit: limit
      }
    }).toPromise()
    return res.results;
  }

  async getPokemonName(url: string) {
    let res = await this.http.get<RootObject>(url).toPromise();
    return res.name;
  }

  async getPokemonType(url: string): Promise<Type[]> {
    let res = await this.http.get<RootObject>(url).toPromise();
    return res.types;
  }

  async getPokemonAbility(url: string): Promise<PokemonAbility[]> {
    let res = await this.http.get<RootObject>(url).toPromise();
    return res.abilities;
  }

  async getPokemonMoves(url: string): Promise<PokemonMove[]> {
    let res = await this.http.get<RootObject>(url).toPromise();
    return res.moves;
  }

  async getPokemonStats(url:string): Promise<PokemonStats[]> {
    let res = await this.http.get<RootObject>(url).toPromise();
    return res.stats;
  }
}
