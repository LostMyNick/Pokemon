import {Component, OnInit} from '@angular/core';
import {Pokemon} from "../class/pokemon";
import {RepositoryService} from "../repository.service";
import {Type} from "../class/pokemon_type";
import {PokemonAbility} from "../class/pokemon_ability";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private repositoryService: RepositoryService) {
  }

  pokemon_list: Array<Pokemon> = [];
  url_list: Array<string> = [];
  type_list: Array<Type[]> = [];
  ability_list: Array<PokemonAbility[]> = [];
  ability_visibility: Array<boolean> = [];
  type_visibility: Array<boolean> = [];
  current_page: number = 1;
  compare_list_id: Array<number> = [];
  compare_list_name: Array<string> = [];
  disable_buttons: boolean = false;

  ngOnInit() {
    this.getPokemonList(0, 20);
  }

  getPokemonList(offset: number, limit: number) {
    this.repositoryService.getPokemon(offset, limit)
      .then(pok => {
        this.pokemon_list = pok;
        this.getUrl(limit);
      })
  }

  async getUrl(limit: number) {
    for (let i = 0; i < limit; i++) {
      this.url_list[i] = this.pokemon_list[i].url;
      await this.repositoryService.getPokemonType(this.pokemon_list[i].url)
        .then(typ => {
          this.type_list[i] = typ;
        });
      await this.repositoryService.getPokemonAbility(this.pokemon_list[i].url)
        .then(abi => {
          this.ability_list[i] = abi;
        });
      this.ability_visibility[i] = false;
      this.type_visibility[i] = false;
    }
  }

  showAbilityList(id: number) {
    this.ability_visibility[id] = !this.ability_visibility[id];
  }

  showTypeList(id: number) {
    this.type_visibility[id] = !this.type_visibility[id];
  }

  getPreviousPage() {
    this.current_page--;
    this.ability_list = [];
    this.type_list = [];
    this.getPokemonList(20 * (this.current_page - 1), 20);
  }

  getNextPage() {
    this.ability_list = [];
    this.type_list = [];
    this.current_page++;
    this.getPokemonList(20 * (this.current_page - 1), 20);
  }

  compareElements(id: number) {
    id++;
    for (let i = 0; i < this.compare_list_id.length; i++) {
      if (this.compare_list_id[i] == id) {
        return false;
      }
    }
    return true;
  }

  addToCompare(id: number) {
    id++;
    let temp_url = this.repositoryService.url + 'pokemon/' + id;
    this.repositoryService.getPokemonName(temp_url).then(name => {
        this.compare_list_name.push(name)
      }
    )
    this.compare_list_id.push((this.current_page - 1) * 20 + id);
    this.disable_buttons = this.compare_list_id.length == 2;
  }

  removeFromCompare(id: number) {
    id++;
    this.compare_list_id.splice(this.compare_list_id.indexOf((this.current_page - 1) * 20 + id), 1);
    this.compare_list_name.splice(this.compare_list_id.indexOf((this.current_page - 1) * 20 + id), 1);
    this.disable_buttons = this.compare_list_id.length == 2;
  }
}
