import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {RepositoryService} from "../repository.service";
import {Type} from "../class/pokemon_type";
import {PokemonAbility} from "../class/pokemon_ability";
import {PokemonMove} from "../class/pokemon_move";
import {PokemonStats} from "../class/pokemon_stats";
import {Router} from "@angular/router";

@Component({
  selector: 'app-compare',
  templateUrl: './compare.component.html',
  styleUrls: ['./compare.component.css']
})
export class CompareComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private http: HttpClient,
              private repositoryService: RepositoryService,
              private router: Router) {
  }

  id: string = '';
  id2: string = '';
  type: Array<Type[]> = [];
  name: Array<string> = [];
  abilities: Array<PokemonAbility[]> = [];
  moves: Array<PokemonMove[]> = [];
  stats: Array<PokemonStats[]> = [];

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
      this.id2 = params['id2'];
    })
    if(this.id == undefined || this.id2 == undefined) {
      this.router.navigate(['home']);
    }
    this.getDetails(this.id)
  }


  async getDetails(id: string) {
    for(let i = 0; i < 2; i++) {
      let url = this.repositoryService.url + 'pokemon/' + id;
      this.name[i] = await this.repositoryService.getPokemonName(url);

      await this.repositoryService.getPokemonType(url).then(type => {
        this.type[i] = type;
      });
      await this.repositoryService.getPokemonAbility(url).then(ability => {
        this.abilities[i] = ability;
      });
      await this.repositoryService.getPokemonMoves(url).then(move => {
        this.moves[i] = move;
      });
      await this.repositoryService.getPokemonStats(url).then(stat => {
        this.stats[i] = stat;
      })
      id = this.id2;
    }
  }
}
