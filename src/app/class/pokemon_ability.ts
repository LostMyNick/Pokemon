import {PokemonAbility2} from "./pokemon_ability2";

export class PokemonAbility {
    constructor(public ability: PokemonAbility2,
                public is_hidden: boolean,
                public slot: number) {
        this.ability = ability;
        this.is_hidden = is_hidden;
        this.slot = slot;
    }
}
