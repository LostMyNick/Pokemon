import {PokemonType2} from "./pokemon_type2";

export class Type {
  constructor(public slot: number, public type: PokemonType2) {
    this.slot = slot;
    this.type = type;
  }
}
