import {Pokemon} from "./pokemon";

export class PokemonList {
  constructor(public next: string,
              public previous: string,
              public results: Pokemon[]) {
    this.next = next;
    this.previous = previous;
    this.results = results;
  }
}
