import {PokemonStats2} from "./pokemon_stats2";

export class PokemonStats {
  constructor(public base_stat: number,
              public stat: PokemonStats2) {
    this.base_stat = base_stat;
    this.stat = stat;
  }
}
