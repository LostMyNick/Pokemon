import {Type} from "./pokemon_type";
import {PokemonAbility} from "./pokemon_ability";
import {PokemonMove} from "./pokemon_move";
import {PokemonStats} from "./pokemon_stats";

export class RootObject {

  // base_experience: number;
  // forms: Form[];
  // game_indices: GameIndice[];
  // height: number;
  // held_items: any[];
  // id: number;
  // is_default: boolean;
  // location_area_encounters: string;
  // order: number;
  // past_types: any[];
  // species: Species;
  // sprites: Sprites;
  // weight: number;
  constructor(public types: Type[],
              public abilities: PokemonAbility[],
              public name: string,
              public moves: PokemonMove[],
              public stats: PokemonStats[]) {
    this.types = types;
    this.abilities = abilities;
    this.name = name;
    this.moves = moves;
    this.stats = stats;
  }
}
