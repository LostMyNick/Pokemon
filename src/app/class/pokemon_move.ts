import {PokemonMove2} from "./pokemon_move2";

export class PokemonMove {
  constructor(public move: PokemonMove2) {
    this.move = move;
  }
}
