import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule} from "@angular/common/http";
import {RouterModule, Routes} from "@angular/router";

import {AppComponent} from './app.component';
import {DetailsComponent} from './details/details.component';
import { HomeComponent } from './home/home.component';
import { CompareComponent } from './compare/compare.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'details', component: DetailsComponent},
  {path: 'compare', component: CompareComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    DetailsComponent,
    HomeComponent,
    CompareComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
