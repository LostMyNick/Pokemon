import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {RepositoryService} from "../repository.service";
import {Type} from "../class/pokemon_type";
import {PokemonAbility} from "../class/pokemon_ability";
import {PokemonMove} from "../class/pokemon_move";
import {PokemonStats} from "../class/pokemon_stats";

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private http: HttpClient,
              private repositoryService: RepositoryService) {
  }

  id: string = '';
  type: Array<Type> = [];
  name: string = '';
  abilities: Array<PokemonAbility> = [];
  moves: Array<PokemonMove> = [];
  stats: Array<PokemonStats> = [];

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.id = params['id'];
    })
    this.getDetails(this.id)
  }


  async getDetails(id: string) {
    let url = this.repositoryService.url + 'pokemon/' + id;
    this.name = await this.repositoryService.getPokemonName(url);

    await this.repositoryService.getPokemonType(url).then(type => {
      this.type = type;
    });
    await this.repositoryService.getPokemonAbility(url).then(ability => {
      this.abilities = ability;
    });
    await this.repositoryService.getPokemonMoves(url).then(move => {
      this.moves = move;
    });
    await this.repositoryService.getPokemonStats(url).then(stat => {
      this.stats = stat;
    })
  }
}
